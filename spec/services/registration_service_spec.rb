# require 'spec_helper'
def require_dependency(*args)
  require(*args)
end

require './app/services/registration_service'

describe RegistrationService do
  valid_hash = {
    username: 'andrew',
    password: 'password', 
    email: 'andru.weir@gmail.com'
  }

  invalid_hash = {
    username: 'andrew', 
    password: '', 
    email: 'andru.weir#gmail.com'
  }

  repo_hash = valid_hash.merge(id: 1, valid?: true, encrypt_password: true, authenticate?: true)

  subject { RegistrationService.new(context, repo) } 
  let(:entity) { double(repo_hash) } 
  let(:context) { double(create_succeeded: nil, create_failed: nil) }

  context "register with valid params" do
    let(:repo) { double(create: entity, save: true) }

    it "should save" do
      subject.register(valid_hash)

      expect(context).to have_received(:create_succeeded)
        .with(entity, 'Registration successful')
    end
  end

  context "register with invalid params" do
    let(:repo) { double(create: entity, save: false) }
    it "should not not save" do
      subject.register(invalid_hash)

      expect(context).to have_received(:create_failed)
        .with(entity, 'There was an issue creating your account')
    end
  end
end