require 'spec_helper'
# require 'capybara/rspec'

feature "Registration process" do
  before(:each) do
    visit register_path   
  end

  scenario "Creating account with correct information" do
    fill_in "Username", with: "andrew"
    fill_in "Password", with: "password"
    fill_in "Password confirmation", with: "password"
    fill_in "Email", with: "andru.weir@gmail.com"

    click_button "Sign up"

    expect(current_path).to eq(root_path)
    expect(page).to have_content('Registration successful')
  end

  scenario "Create account with incorrect information" do
    fill_in "Username", with: "andrew"
    fill_in "Password", with: ""
    fill_in "Password confirmation", with: "different"
    fill_in "Email", with: "andru.weir#gmail.com"

    click_button "Sign up"

    expect(current_path).to eq(users_path)
    expect(page).to have_content('There was an issue creating your account')
    expect(page).to have_content('password is shorter than 8 characters')
    expect(page).to have_content('password does not match password confirmation')
    expect(page).to have_content('email is not valid')
  end
end