require 'spec_helper'
require 'bcrypt'

feature "Login" do
  before(:each) do
    User.create email: 'andru.weir@gmail.com', password_digest: BCrypt::Password.create('password')
    visit login_path
  end

  scenario "should login with correct credentials" do
    fill_in "Email", with: "andru.weir@gmail.com"
    fill_in "Password", with: "password"

    click_button "Login"

    expect(page).to have_content('Login successful')
  end

  scenario "should not login with incorrect credentials" do
    fill_in "Email", with: "andru.weir@gmail.com"
    fill_in "Password", with: "badpass"

    click_button "Login"

    expect(page).to have_content('Email and password do not match')
  end
end