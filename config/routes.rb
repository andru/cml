Cml::Application.routes.draw do
	root to: 'pages#home'

  get '/register', to: 'users#new', as: 'register'
  resources :users, only: ['create']

  get '/login', to: 'sessions#new', as: 'login'
  resources :sessions, only: ['create', 'destroy']
end
