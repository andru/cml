class UserRepository
  def create(params = {})
    UserEntity.new(params)
  end

  def save(user)
    if user.valid?
      User.create!(user.attributes)
    end
  end

  def find_by_email(email)
    entity_from_db(User.where(email: email).first)
  end

  private

  def entity_from_db(db_user)
    if db_user
      UserEntity.new(db_user.attributes)
    end
  end
end