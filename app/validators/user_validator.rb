class UserValidator
  include Veto.validator

  validates :username, format: RubyRegex::Username
  validates :password, min_length: 8
  validates :email, format: RubyRegex::Email
  validate :passwords_match?

  def passwords_match?
    if entity.passwords_match?
      errors.add(:password, 'does not match password confirmation')
    end
  end
end