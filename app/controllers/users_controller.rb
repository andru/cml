class UsersController < ApplicationController
  before_filter :init

  def new
    @user = @user_repository.create
  end

  def create
    @registration_service.register(params[:user])
  end

  def create_succeeded(user, message)
    redirect_to root_path, notice: message
  end

  def create_failed(user, message)
    @user = user
    flash[:notice] = message
    render :new
  end

  private

  def init
    @user_repository = UserRepository.new
    @registration_service = RegistrationService.new(self, @user_repository)
  end
end
