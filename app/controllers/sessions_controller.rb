class SessionsController < ApplicationController
  before_filter :init

  def new
    @user = @user_repository.create
  end

  def create
    @login_service.login(params[:user])
  end

  def login_succeeded(message)
    redirect_to root_path, notice: message
  end

  def login_failed(user, message)
    @user = user
    flash[:notice] = message
    render :new
  end

  private

  def init
    @user_repository = UserRepository.new
    @login_service = LoginService.new(self, @user_repository)
  end
end
