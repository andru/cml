class LoginService
  def initialize(context, user_repository)
    @context = context
    @user_repository = user_repository
  end

  def login(params)
    user = @user_repository.find_by_email(params[:email])
    
    if user && user.authenticate?(params[:password])
      @context.login_succeeded('Login successful')
    else
      @context.login_failed(@user_repository.create(params), 'Email and password do not match')
    end
  end
end