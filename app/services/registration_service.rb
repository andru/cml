class RegistrationService
  def initialize(context, user_repository)
    @context = context
    @user_repository = user_repository
  end

  def register(params)
    user = @user_repository.create(params)
    user.encrypt_password

    if @user_repository.save(user)
      @context.create_succeeded(user, 'Registration successful')
    else
      @context.create_failed(user, 'There was an issue creating your account')
    end
  end
end