require 'BCrypt'

module UserAuthenticator
  include BCrypt
  
  def passwords_match?
    password_confirmation && password != password_confirmation
  end

  def encrypt_password
    self.password_digest = Password.create(@password)
  end

  def authenticate?(password)
    Password.new(@password_digest) == password
  end
end