class UserEntity
  include UserAuthenticator  
  include Virtus.model
  include Veto.model(UserValidator)

  attr_accessor :password, :password_confirmation

  attribute :id, Integer
  attribute :username, String
  attribute :password_digest, String
  attribute :email, String
  attribute :created_at, DateTime
  attribute :updated_at, DateTime
end